package br.ifsc.edu.dudafire;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

        //mAuth.createUserWithEmailAndPassword("dudasouzalima01@gmail.com", "123bla");
        // mAuth.signInWithEmailAndPassword("","");

        mAuth.signInWithEmailAndPassword("dudasouzalima01@gmail.com", "123bla").addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText((getApplicationContext()),mAuth.getCurrentUser().getEmail(),Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(), "Falha no login", Toast.LENGTH_LONG).show();
                }
            }
        });

        FirebaseUser firebaseUser = mAuth.getCurrentUser();

        if (firebaseUser != null) {
            Log.d("FirebaseUserExemplo", "Usúario Logado" + firebaseUser.getEmail());
        } else {
            Log.d("FirebaseUserExemplo", "Falha na autenticação");
        }
    }
}
